vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv('HOME') .. '/.nvim/undotree/'
vim.opt.undofile = true

vim.opt.mouse = '' -- Enable mouse in no modes

vim.opt.completeopt = { 'menuone', 'noselect' }

vim.opt.smartindent = true -- Auto indenting

vim.opt.signcolumn = 'yes'
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.colorcolumn = '80'

vim.opt.wrap = false
vim.opt.breakindent = true

vim.opt.splitbelow = true
vim.opt.splitright = true

vim.opt.guicursor = '' -- Always block cursor
vim.o.termguicolors = true
vim.cmd('colorscheme habamax')
vim.opt.listchars = 'tab:» ,trail:~,extends:›,precedes:‹'
vim.opt.list = true

vim.g.netrw_banner = 0
-- TODO fold options
