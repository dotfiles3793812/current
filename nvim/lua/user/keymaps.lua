vim.g.mapleader = ' '
vim.keymap.set('n', '<leader>fe', vim.cmd.Ex)

-- Cursor stable line joining
vim.keymap.set('n', 'J', 'mzJ`z')

-- Center cursor when moving
vim.keymap.set('n', '<C-d>', '<C-d>zz')
vim.keymap.set('n', '<C-u>', '<C-u>zz')
vim.keymap.set('n', '{', '{zz')
vim.keymap.set('n', '}', '}zz')
vim.keymap.set('n', '%', '%zz')
vim.keymap.set('n', '*', '*zzzv')
vim.keymap.set('n', '#', '#zzzv')
vim.keymap.set('n', 'n', 'nzzzv')
vim.keymap.set('n', 'N', 'nzzzv')

-- Yank to system clipboard
vim.keymap.set('n', '<leader>y', '\'+y')
vim.keymap.set('v', '<leader>y', '\'+y')
vim.keymap.set('n', '<leader>Y', '\'+Y')

-- Delete to void
vim.keymap.set('n', '<leader>d', '\'_d')
vim.keymap.set('v', '<leader>d', '\'_d')

-- undotree
vim.keymap.set('n', '<leader>u', vim.cmd.UndotreeToggle)

-- telescope fzf
local telescope = require('telescope.builtin')
vim.keymap.set('n', '<C-p>', telescope.git_files, {})
vim.keymap.set('n', '<leader>fd', telescope.find_files, {})
vim.keymap.set('n', '<leader>fs', function()
    telescope.grep_string({ search = vim.fn.input(':fzf grep ') });
end)
