return {
    {
	'nvim-telescope/telescope.nvim', tag = '0.1.x',
	-- or			       , branch = '0.1.x',
	dependencies = {
	    'nvim-lua/plenary.nvim',
	    'BurntSushi/ripgrep',
	    'sharkdp/fd',
	    {
		'nvim-telescope/telescope-fzf-native.nvim',
		build = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build',
		cond = vim.fn.executable('cmake') == 1,
	    },
	},
	config = function()
	    local actions = require('telescope.actions')

	    require('telescope').setup({
		defaults = {
		    mappings = {
			i = {
			    ['<C-j>'] = actions.move_selection_next,
			    ['<C-k>'] = actions.move_selection_previous,
			},
		    },
		    hidden = true,
		},
	    })

	    -- Enable telescope fzf native, if installed
	    pcall(require('telescope').load_extension, 'fzf')
	end,
    },
}
